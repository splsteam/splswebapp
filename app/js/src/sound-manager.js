// Web Wall Whispers
//
// Interactive web-based audio-visual installation.
// Developed for Fondazione Spinola-Banna per l'Arte.
// Copyright (C) 2018 Francesco Cretti
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see
// https://bitbucket.org/splsteam/splswebapp/src/master/LICENSE.txt.
//
// JS developer - francesco.cretti@gmail.com
// Coordinator - leehooni@gmail.com
//
// Home Page: www.webwallwhispers.net
// Bug report: https://bitbucket.org/splsteam/splswebapp/issues

/**
 * @function BufferLoader BUFFER LOADER WITH AJAX REQUEST
 * @param {Object} audioContext WebAudio Context of current application
 * @param {Array} urlList Array of URL strings for audio files to be loaded
 * @param {Function} callback Callback function to be called at the end of loading
 **/
function BufferLoader(audioContext, urlList, callback) {
		this.context = audioContext;
    this.urlList = urlList;
    this.onload = callback;
    this.bufferList = [];
    this.loadCount = 0;
}

/**
 * @function loadBuffer Request array buffer with AJAX call, decode audio data and puts in buffer
 * Call callback function when finished passing all buffers and audio context
 * @param {String} url url to file to retrieve
 * @param {Number} index
 **/
BufferLoader.prototype.loadBuffer = function (url, index) {
  const loader = this;
	var request = new XMLHttpRequest();
  request.open('GET', url, true);
  request.responseType = 'arraybuffer';
  request.onload = function () {
    loader.context.decodeAudioData(request.response, function (buffer) {
			if (!buffer) {
				alert(`Error decoding file data ${url}`);
				return;
			}
			loader.bufferList[index] = buffer;
      if (++loader.loadCount === loader.urlList.length) {
				loader.onload(loader.bufferList, loader.context);
			}
		}, function (e) {console.log('decoding error');});
	};
	request.onerror = function () {
		alert('BufferLoader: XHR error');
	};
	request.send();
};

/**
 * @function load Load and decode all buffers in url list
 **/
BufferLoader.prototype.load = function () {
  for (var i = 0; i < this.urlList.length; ++i) {
		this.loadBuffer(this.urlList[i], i);
	}
};

// *********************************************************
/**
 * @function ActionSound ACTION SOUND CONSTRUCTOR
 * @param {Object} AudioContext WebAudio Context of current application
 * @param {ArrayBuffer} bufferSource Actual array buffer with decoded audio data
 * @param {Number} idx Sound index
 * @param {Object} HTMLlog DOM element for logging audio status
 * @param {Boolean} debugLog True to activate console debug logs
 **/
function ActionSound(audioContext, bufferSource, idx, HTMLlog, debugLog) {
	this.context = audioContext;
  this.buffer = bufferSource;
	this.index = idx;
	this.htmlLogger = HTMLlog;
	this.dLog = debugLog;
	this.sourceNode = '';
  this.masterGain = '';
	this.muteNode = '';
  this.playing = false;
	this.loop = false;
}

/**
 * @function init Create nodes and do connections
 **/
ActionSound.prototype.init = function () {
	this.muteNode = this.context.createGain();
	this.masterGain = this.context.createGain();
	this.muteNode.connect(this.masterGain);
	this.masterGain.connect(this.context.destination);
	this.masterGain.gain.setTargetAtTime(1, this.context.currentTime, 0.5);
	// console.log('ACTION SOUND INIT');
};

/**
 * @function play Connect and play
 **/
ActionSound.prototype.play = function () {
	if (!this.playing) {
		this.sourceNode = this.context.createBufferSource();
		this.sourceNode.loop = this.loop;
		this.sourceNode.buffer = this.buffer;
		this.sourceNode.start(0);
		this.sourceNode.connect(this.muteNode);
		this.playing = true;
		this.dLog && console.log(`Action sound ID: ${this.index} - Play`);
		// this.htmlLogger.innerHTML += '<br>Played action sound ID: ' + this.index;
	} else {
		// this.dLog && console.log('Already playing. Check gain value');
	}
};

/**
 * @function stop Stop and disconnect
 **/
ActionSound.prototype.stop = function () {
	if (this.playing) {
		this.sourceNode.stop(0);
		this.sourceNode.disconnect(this.muteNode);
		this.playing = false;
		this.dLog && console.log(`Action sound ID: ${this.index} - stopped`);
		this.htmlLogger.innerHTML += '<br>Stopped action sound <ID: ' + this.index + '> ';
	}
};

/**
 * @function setLoop
 * @param {Boolean} loop
 **/
ActionSound.prototype.setLoop = function (loop) {
	const self = this;
	// Sets loop when starts playing
	self.loop = loop;
	// Useful when already playing
	self.sourceNode.loop = loop;
	self.dLog && console.log(`Action sound ID: ${this.index} - loop set to ${loop}`);
};

/**
 * @function fire Play and disconnect after finish
 **/
ActionSound.prototype.fire = function () {
	const self = this;
	self.sourceNode = self.context.createBufferSource();
	self.sourceNode.buffer = self.buffer;
	self.sourceNode.start(0);
	self.sourceNode.connect(self.muteNode);
	self.playing = true;
	self.dLog && console.log(`Action sound ID: ${this.index} - fired!`);
};

/**
 * @function fadeIn
 * @param {Number} fadeTime Duration of fade in seconds
 **/
ActionSound.prototype.fadeIn = function (fadeTime) {
	if (!this.playing) {
		if (!fadeTime) {
			this.dLog && console.warn(`Missing argument fadeTime. Set to 1 second`);
			fadeTime = 1;
		}
		this.masterGain.gain.setValueAtTime(0, this.context.currentTime);
		this.play();
		this.masterGain.gain.linearRampToValueAtTime(1, this.context.currentTime + fadeTime);
		this.htmlLogger.innerHTML += '<br>Action sound fading in <ID: ' + this.index + '>... ';
	} else {
		// this.dLog && console.log("Already playing. Check gain value.");
	}
};

/**
 * @function fadeOut
 * @param {Number} fadeTime Duration of fade in seconds
 **/
ActionSound.prototype.fadeOut = function (fadeTime) {
	const self = this;
	if (self.playing) {
		if (!fadeTime) {
			self.dLog && console.warn(`Missing argument fadeTime. Set to 1 second`);
			fadeTime = 1;
		}
		self.masterGain.gain.setValueAtTime(self.masterGain.gain.value, self.context.currentTime);
		self.masterGain.gain.linearRampToValueAtTime(0.0001, self.context.currentTime + fadeTime);
		self.htmlLogger.innerHTML += '<br>Action sound fading out <ID: ' + self.index + '>... ';
		setTimeout(function () {
			self.stop();
		}, fadeTime * 1000);
	}
};

/**
 * @function mute
 **/
ActionSound.prototype.mute = function () {
	this.muteNode.gain.setValueAtTime(0, this.context.currentTime);
};

/**
 * @function unmute
 **/
ActionSound.prototype.unmute = function () {
		this.muteNode.gain.setValueAtTime(1, this.context.currentTime);
};

/**
 * @function fadeToGain
 * @param {Number} val Gain value to fade to
 * @param {Number} fadeTime Fade time in seconds
 **/
ActionSound.prototype.fadeToGain = function (val, fadeTime) {
		if (val > 1) {
			this.dLog && console.warn('Gain values cannot be greater than 1');
			val = 1;
		} else if (val < 0) {
			this.dLog && console.warn('Gain values cannot be lower than 0');
			val = 0;
		}
		if (!fadeTime) {
			this.dLog && console.warn(`Missing argument fadeTime. Set to 1 second`);
			fadeTime = 1;
		}
		this.masterGain.gain.setValueAtTime(this.masterGain.gain.value, this.context.currentTime);
		this.masterGain.gain.linearRampToValueAtTime(val, this.context.currentTime + fadeTime);
};


// *********************************************************
/**
 * @function StreamSound stream sound constructor
 * @param {Object} AudioContext WebAudio Context of current application
 * @param {Number} idx Sound index
 * @param {Object} HTMLlog DOM element for logging audio status
 * @param {Boolean} debugLog True to activate console debug logs
 **/
function StreamSound(audioContext, idx, HTMLlog, debugLog, mobile) {
	this.context = audioContext;
	this.index = idx;
	this.htmlLogger = HTMLlog;
	this.dLog = debugLog;
	this.mobile = mobile;
	this.secondaryStream = undefined;
	this.secondary = false;
	this.audioElement = undefined;
	this.hls = undefined;
	this.playing = false;
	this.fadingIn = false;
	this.fadingOut = false;
	this.hlsLoadInit = false;
	this.sourceNode = undefined;
	this.masterGain = undefined;
	this.reverb = false;
	this.reverberator = undefined;
	this.gainRandom = false;
	this.randomizeGain = false;
	if (!mobile) {
		this.filter = new Filter(audioContext, this);
		this.distortion = new Distortion(audioContext, this);
		this.binauralizator = new Binauralizator(audioContext, this);
	} else {
		this.stereoPanner = new StereoPanner(audioContext, this);
	}
}

/**
 * @function init
 * @param {Boolean} autoStart
 *			If true Hls starts immediately to load fragments (if the associated
 *			audioElement is paused, it stops after 5 frags)
 *			If false < hls.startLoad(startPosition=-1) > is necessary later
 * @param {Number} folderNumber
 * @param {Boolean} mobile
 * @param {String} dirURL
 **/
StreamSound.prototype.init = function (autoStart, folderNumber, dirURL) {
	'use strict';

	const self = this;
	self.audioElement = new Audio();
	self.hls = new Hls();
	var streamsDir;
	if (self.mobile) {
		streamsDir = '/audio/streamsHEAAC/';
	} else {
		streamsDir = '/audio/streams/';
	}
	var sourceURL = dirURL.concat(streamsDir, 'stream', folderNumber.toString(),
	 '/', 'master', folderNumber.toString(), '.m3u8');
	self.hls.loadSource(sourceURL);
	self.hls.attachMedia(self.audioElement);
	self.hls.config.autoStartLoad = autoStart;
	self.audioElement.crossOrigin = 'anonymous';
	self.audioElement.loop = true;
	self.sourceNode = self.context.createMediaElementSource(self.audioElement);
	self.masterGain = self.context.createGain();
	self.masterGain.gain.setValueAtTime(0, self.context.currentTime);
	self.sourceNode.connect(self.masterGain);
	self.masterGain.connect(self.context.destination);
	self.audioElement.pause();
	// self.dLog && console.log(`Stream Sound ${self.index} loaded with Hls`);
	self.hls.on(Hls.Events.FRAG_PARSING_INIT_SEGMENT, function () {
		self.dLog && console.log(`Init segment parsed audio ${self.index}`);
		self.hlsLoadInit = true;
	});
};

/**
 * @function initSecondary
 * @param {StreamSound} origStream
 **/
StreamSound.prototype.initSecondary = function (origStream) {
	const self = this;
	self.secondary = true;
	// Hold reference to secondary stream
	self.secondaryStream = origStream;
	origStream.secondaryStream = self;
	self.audioElement = origStream.audioElement;
	self.masterGain = self.context.createGain();
	self.masterGain.gain.setValueAtTime(0, self.context.currentTime);
	// origStream.masterGain.gain.setValueAtTime(0, self.context.currentTime);
	origStream.sourceNode.connect(self.masterGain);
	self.masterGain.connect(self.context.destination);
	self.dLog && console.log(`Secondary stream ${self.index} derived from stream ${origStream.index}`);
};

/**
 * @function isPlaying
 * @returns {Boolean} True if stream is currently playing
 **/
StreamSound.prototype.isPlaying = function () {
	// return (!this.audioElement.paused && this.audioElement.currentTime > 0 &&
	// 	!this.audioElement.ended);
	return this.playing;
};

/**
 * @function isFadingIn
 * @returns {Boolean} True if stream is currently fading in
 **/
StreamSound.prototype.isFadingIn = function () {
	return this.fadingIn;
};

/**
 * @function isFadingOut
 * @returns {Boolean} True if stream is currently fading out
 **/
StreamSound.prototype.isFadingOut = function () {
	return this.fadingOut;
};

/**
 * @function isFading
 * @returns {Boolean} True if stream is currently fading in or out
 **/
StreamSound.prototype.isFading = function () {
	return (this.fadingOut || this.fadingIn);
};

/**
 * @function play
 **/
StreamSound.prototype.play = function () {
	this.audioElement.play();
	this.playing = true;
};

/**
 * @function pause
 **/
StreamSound.prototype.pause = function () {
	this.audioElement.pause();
	this.playing = false;
};

/**
 * @function fadeIn
 * @param {Number} fadeTime Fade time in seconds
 * @param {Boolean}  random If true audio will fade in at a random starting time
 **/
StreamSound.prototype.fadeIn = function (fadeTime, random, randomTime) {
	const self = this;
	if (!self.isPlaying()) {
		if (!fadeTime) {
			self.dLog && console.warn(`Missing argument fadeTime. Set to 1 second`);
			fadeTime = 1;
		}
		if (self.randomizeGain && !self.gainRandom) {
			self.gainRandomizerStart();
		}
		if (random) {
			if (randomTime === undefined || randomTime < 0 || randomTime > self.audioElement.duration) {
				var randomTime = Math.floor(Math.random() * (self.audioElement.duration + 1));
			}
			// control if cannot set random current time (resource not loaded yet)
			if (!isNaN(randomTime)) {
				self.dLog && console.log(`Random starting time: ${randomTime} sec for stream ${self.index}`);
				// self.seekTime = performance.now();
				self.audioElement.currentTime = randomTime;
			}
		}
		// If has not playing secondary
		if (self.secondaryStream === undefined) {
			// Start playing
			self.dLog && console.log(`No secondary streams -> play`);
			self.play();
		} else if (!self.secondaryStream.isPlaying()) {
			self.dLog && console.log(`Not playing secondary streams -> play`);
			self.play();
		} else {
			self.playing = true;
		}
		var curTime = self.context.currentTime;
		self.masterGain.gain.cancelScheduledValues(curTime);
		self.masterGain.gain.setValueAtTime(0.0001, curTime);
		self.masterGain.gain.linearRampToValueAtTime(1.0, curTime + fadeTime);
		self.fadingIn = true;
		setTimeout(function () {
			self.fadingIn = false;
			self.dLog && console.log(`Fade in stream finished [ ID: ${self.index} ]`);
			self.htmlLogger.innerHTML += '<br>Fade in stream finished [ID: ' + self.index + '] ';
		}, fadeTime * 1000 + 25);
		self.dLog && console.log(`Fading in stream [ ID: ${self.index} ]`);
		self.htmlLogger.innerHTML += '<br>Fading in stream [ID: ' + self.index + ']... ';
	} else {
		self.dLog && console.warn(`Cannot fade in. Sound already playing [ ID: ${self.index} ]`);
	}
};

/**
 * @function fadeOut
 * @param {Number} fadeTime Fade time in seconds
 **/
StreamSound.prototype.fadeOut = function (fadeTime) {
	const self = this;
	if (self.isPlaying()) {
		if (!fadeTime) {
			self.dLog && console.warn(`Missing argument fadeTime. Set to 1 second`);
			fadeTime = 1;
		}
		if (self.gainRandom) {
			self.gainRandomizerStop();
		}
		var currGain = self.masterGain.gain.value;
		var curTime = self.context.currentTime;
		self.masterGain.gain.cancelScheduledValues(curTime);
		self.masterGain.gain.setValueAtTime(currGain, curTime);
		self.masterGain.gain.linearRampToValueAtTime(0.0001, curTime + fadeTime);
		self.fadingOut = true;
		setTimeout(function () {
			// If has not secondary streams
			if (self.secondaryStream === undefined) {
				self.dLog && console.log(`No secondary streams -> stop`);
				self.pause();
			} else if (!self.secondaryStream.isPlaying()) {
				self.dLog && console.log(`Not playing secondary streams -> stop`);
				self.pause();
			} else {
				self.dLog && console.log(`Playing secondary streams`);
				self.playing = false;
			}
			self.fadingOut = false;
			self.dLog && console.log(`Fade out stream finished [ ID: ${self.index} ]`);
			self.htmlLogger.innerHTML += '<br>Fade out stream finished [ID: ' + self.index + '] ';
		}, fadeTime * 1000 + 25);
		self.dLog && console.log(`Fading out stream [ ID: ${self.index} ]`);
		self.htmlLogger.innerHTML += '<br>Fading out stream [ID: ' + self.index + ']... ';
	} else {
		self.dLog && console.warn(`Cannot fade out. Sound not playing [ ID: ${self.index} ]`);
	}
};

/**
 * @function fadeToGain
 * @param {Number} val Gain value to fade to
 * @param {Number} fadeTime Fade time in seconds
 **/
StreamSound.prototype.fadeToGain = function (val, fadeTime) {
		if (val > 1) {
			this.dLog && console.warn('Gain values cannot be greater than 1');
			val = 1;
		} else if (val < 0) {
			this.dLog && console.warn('Gain values cannot be lower than 0');
			val = 0;
		}
		if (!fadeTime) {
			this.dLog && console.warn(`Missing argument fadeTime. Set to 1 second`);
			fadeTime = 1;
		}
		this.masterGain.gain.cancelScheduledValues(this.context.currentTime);
		this.masterGain.gain.setValueAtTime(this.masterGain.gain.value, this.context.currentTime);
		this.masterGain.gain.linearRampToValueAtTime(val, this.context.currentTime + fadeTime);
};

/**
 * @function setMasterGain
 * @param {Number} mgain Gain value of master signal
 **/
StreamSound.prototype.setMasterGain = function (mgain) {
	if (mgain > 1) {
		this.dLog && console.warn('Gain values cannot be greater than 1');
		mgain = 1;
	} else if (mgain < 0) {
		this.dLog && console.warn('Gain values cannot be lower than 0');
		mgain = 0;
	}
	this.masterGain.gain.setTargetAtTime(mgain, this.context.currentTime, 0.5);
};

/**
 * @function getMasterGain
 * @returns {Number} Master gain value
 **/
StreamSound.prototype.getMasterGain = function () {
	return this.masterGain.gain.value;
};

/**
 * @function getDuration
 * @returns {Number} track duration
 **/
StreamSound.prototype.getDuration = function () {
	return this.audioElement.duration;
};

/**
 * @function mute
 **/
StreamSound.prototype.mute = function () {
	this.audioElement.muted = true;
};

/**
 * @function unmute
 **/
StreamSound.prototype.unmute = function () {
	this.audioElement.muted = false;
};

/**
 * @function startFragmentsLoading
 **/
StreamSound.prototype.startFragmentsLoading = function () {
	if (!this.hlsLoadInit) {
		this.hls.startLoad(startPosition = -1);
		// this.dLog && console.log(`Started hls fragments loading audio ${this.index}`);
	} else {
		// this.dLog && console.log(`Hls already initialized for audio ${this.index}`);
	}
};

/**
 * @function addReverb
 * @param {Object} rvb Reverberator object to connect to
 * @param {Number} wgain Gain value of wet signal
 * @returns {Boolean} True if reverb added / Flase if already present
 **/
StreamSound.prototype.addReverb = function (rvb, wgain) {
	const self = this;
	if (!self.reverb) {
		if (wgain > 1) {
			self.dLog && console.warn('Gain values cannot be greater than 1');
			wgain = 1;
		} else if (wgain < 0) {
			self.dLog && console.warn('Gain values cannot be lower than 0');
			wgain = 0;
		}
		self.reverberator = rvb;
		// create gain node for adjusting wet signal
		self.wetGain = self.context.createGain();
		self.wetGain.gain.setTargetAtTime(wgain, self.context.currentTime, 0.5);
		// Do connections
		self.masterGain.connect(self.wetGain);
		self.wetGain.connect(self.reverberator.convolver);
		self.reverb = true;
		self.dLog && console.log(`Reverb added [ID: ${self.index}]`);
		return true;
	} else {
		self.dLog && console.warn(`Cannot add reverb. Sound already reverberated [ID: ${self.index}]`);
		return false;
	}
};

/**
 * @function removeReverb
 * @returns {Boolean} True if reverb removed / False if not
 **/
StreamSound.prototype.removeReverb = function () {
		if (this.reverb) {
			this.masterGain.disconnect(this.wetGain);
			this.wetGain.disconnect(this.reverberator.convolver);
			this.dLog && console.log(`Reverb removed [ID: ${this.index}]`);
			this.reverb = false;
			return true;
		} else {
			this.dLog && console.warn(`Cannot remove reverb. Sound not reverberated [ID: ${this.index}]`);
			return false;
		}
};

/**
 * @function setWetGain
 * @param {Number} wgain Gain value for wet signal
 **/
StreamSound.prototype.setWetGain = function (wgain) {
	if (this.reverb) {
		this.wetGain.gain.setTargetAtTime(wgain, this.context.currentTime, 0.5);
	} else {
		this.dLog && console.warn(`Cannot set wet gain. Sound not reverberated [ID: ${this.index}]`);
	}
};

/**
 * @function fadeOutReverb
 * @param {Number} fadeTime Fade time in seconds
 **/
StreamSound.prototype.fadeOutReverb = function (fadeTime) {
	const self = this;
	if (!fadeTime) {
		this.dLog && console.warn(`Missing argument fadeTime. Set to 1 second`);
		fadeTime = 1;
	}
	if (self.reverb) {
		self.wetGain.gain.setValueAtTime(self.wetGain.gain.value, self.context.currentTime);
		self.wetGain.gain.linearRampToValueAtTime(0.0001, self.context.currentTime + fadeTime);
		setTimeout(function () {
			self.removeReverb();
		}, fadeTime * 1000);
	} else {
		self.dLog && console.warn(`Cannot fade out reverb. Sound not reverberated [ID: ${self.index}]`);
	}
};

/**
 * @function stopSprite
 * @param {Number} startTime
 * @param {Number} startTime
 * @param {Boolean} loop
 **/
StreamSound.prototype.playSprite = function (startTime, stopTime, loop) {
	const self = this;
	if (stopTime > self.getDuration()) {
		var spriteDur = stopTime - startTime;
		stopTime = self.getDuration() - 2;
		startTime = stopTime - spriteDur - 2;
	}
	if (!self.isPlaying()) {
		self.audioElement.currentTime = startTime;
		self.fadeIn(1);
	}
	// Check current time every 1 secs
	self.spriteChecker = setInterval(function() {
		self.dLog && console.log(`checking sprite time ${self.audioElement.currentTime} ID:${self.index}`);
		if (!self.isPlaying()) {
			clearInterval(self.spriteChecker);
		}
		if (self.audioElement.currentTime > stopTime) {
			self.dLog && console.log(`sprite finished, fading out`);
			if (loop) {
				self.audioElement.currentTime = startTime;
				self.dLog && console.log(`loooooooooooop! ${startTime}`);
			} else {
				self.fadeOut(5);
				clearInterval(self.spriteChecker);
			}
		}
	}, 1000);
};

/**
 * @function stopSprite
 **/
StreamSound.prototype.stopSprite = function () {
	clearInterval(this.spriteChecker);
	this.fadeOut(1);
};

/**
 * @function gainRandomizerStart produces random sequence to module master gain
 **/
StreamSound.prototype.gainRandomizerStart = function () {
	const self = this;
	if (!self.gainRandom) {
		self.gainRandom = true;
		var i = 0;
		(function set(){
			//var gain = (Math.sin(self.context.currentTime) + 1) * 0.375 + 0.25;
			var gain = Math.random();
			self.dLog &&  console.log(`random gain = ${gain} ID: ${self.index}`);
			// self.fadeToGain(gain, 1);
			self.setMasterGain(gain);
			self.gainRandomId = setTimeout(set, 5000);
			//console.log(`oscill ID ${self.gainOscillId}`);
		}());
	}
};

/**
 * @function gainRandomizerStop
 **/
StreamSound.prototype.gainRandomizerStop = function () {
	if (this.gainRandom) {
		clearTimeout(this.gainRandomId);
		this.setMasterGain(1);
		this.gainRandom = false;
	}
};

/**
 * @function setEffectChain
 * @param {Object} options
 * @param {Object} rvb Reverberator object to connect to
 **/
StreamSound.prototype.setEffectChain = function (options, rvb) {
	// Default values
	let defaults = {
		randomizeGain: false,
		lpFilter: true,
		lpFreq: 10000,
		hpFilter: false,
		hpFreq: 500,
		reverb: true,
		wetGain: 0.5,
		distortion: false,
		distAmount: 50,
		binaural: false,
		stereoPanner : false
	};
	let actual = Object.assign({}, defaults, options);
	/**
	 * Effect chain is always connected as follows:
	 * Source (Master Gain) -> LP filter -> HP filter -> Dist -> Binaural
	 * irrespective of their instertion order
	 **/
	 const self = this;
	 // Randomize gain
	 if (actual.randomizeGain) {
		 // self.gainRandomizerStart();
		 self.randomizeGain = true;
	 } else {
		 // self.gainRandomizerStop();
		 self.randomizeGain = false;
	 }
	 // StereoPanner
	 if (self.mobile) {
		 if (actual.stereoPanner) {
			 self.stereoPanner.addSteroPanner();
		 } else {
			 self.stereoPanner.removeStereoPanner();
		 }
	 } else {
		 // Binaural
		 if (actual.binaural) {
			 self.binauralizator.makeBinaural();
		 } else {
			 self.binauralizator.removeBinaural();
		 }

		 // Low pass filter
		 setTimeout(self.checkLPfilter.bind(self, actual), 300);
		 // High pass filter
		 setTimeout(self.checkHPfilter.bind(self, actual), 600);
		 // Distortion
		 setTimeout(self.checkDistortion.bind(self, actual), 900);
		 // Reverb is independent
		 setTimeout(self.checkReverb.bind(self, actual, rvb), 1200);
	 }
	 self.dLog && console.log(`Effect chain schduled [ID: ${self.index}]`);
 };

 /**
  * @function checkLPfilter
	* @param {Object} options filter parameters
  **/
 StreamSound.prototype.checkLPfilter = function (options) {
	 // this.dLog && console.log('Checking LP filter...');
	 if (options.lpFilter) {
		 // this.dLog && console.log(`Setting LP filter to ${options.lpFreq}...`);
		 if (!this.filter.lowPassFilter(options.lpFreq)) {
			 // If filter already present fade to target frequency
			 this.filter.fadeToLPFrequency(options.lpFreq, 3);
		 }
	 } else {
		 this.filter.removeLowPassFilter();
	 }
 };

 /**
  * @function checkHPfilter
	* @param {Object} options filter parameters
  **/
 StreamSound.prototype.checkHPfilter = function (options) {
	 // this.dLog && console.log('Checking HP filter...');
	 if (options.hpFilter) {
		 // this.dLog && console.log(`Setting HP filter to ${options.hpFreq}...`);
		 if (!this.filter.highPassFilter(options.hpFreq)) {
			 // If filter already present fade to target frequency
			 this.filter.fadeToHPFrequency(options.hpFreq, 3);
		 }
	 } else {
		 // this.dLog && console.log('Removing HP filter...');
		 this.filter.removeHighPassFilter();
	 }
 };

 /**
  * @function checkDistortion
	* @param {Object} options distortion parameters
  **/
 StreamSound.prototype.checkDistortion = function (options) {
	 // this.dLog && console.log('Checking Distortion...');
	 if (options.distortion) {
		 // this.dLog && console.log(`Setting Distortion to ${options.distAmount}...`);
		 if (!this.distortion.addDistortion(options.distAmount)) {
			 // If distortion already present, change amount value
			 this.distortion.setDistAmount(options.distAmount);
		 }
	 } else {
		 this.distortion.removeDistortion();
	 }
 };

 /**
  * @function checkReverb
	* @param {Object} options reverb parameters
	* @param {Object} rvb reverberator object
  **/
 StreamSound.prototype.checkReverb = function (options, rvb) {
	 // this.dLog && console.log('Checking Reverb...');
	 if (options.reverb) {
		 if (!this.addReverb(rvb, options.wetGain)) {
			 // If reverb already present change wet gain
			 this.setWetGain(options.wetGain);
		 }
	 } else {
		 this.fadeOutReverb(3);
	 }
 };


// *********************************************************
// *********************************************************
/**
 * @function Filter filter module constructor
 * @param {Object} AudioContext WebAudio Context of current application
 * @param {Object} parentNode parent audio AudioNode
 **/
function Filter(audioContext, parentNode) {
	'use strict';
	this.context = audioContext;
	this.parent = parentNode;
	this.dryGain = this.context.createGain();
	this.filtered = false;
	this.LPfilter = this.context.createBiquadFilter();
	this.LPfilter.type = 'lowpass';
	this.LPfiltered = false;
	this.HPfilter = this.context.createBiquadFilter();
	this.HPfilter.type = 'highpass';
	this.HPfiltered = false;
	this.LPfading = false;
	this.HPfading = false;
}

/**
 * @function setDryGain
 * @param {Number} dgain Gain value of dry signal
 **/
Filter.prototype.setDryGain = function (dgain) {
		if (this.filtered) {
			this.dryGain.gain.setTargetAtTime(dgain, this.context.currentTime, 0.5);
		} else {
			this.parent.dLog && console.warn(`Add filter before trying to adjust dry gain [ID: ${this.parent.index}]`);
		}
};

/**
 * @function fadeToDryGain
 * @param {Number} val Gain value to fade to
 * @param {Number} fadeTime Fade time in seconds
 **/
Filter.prototype.fadeToDryGain = function (val, fadeTime) {
	if (this.filtered) {
		if (val > 1) {
			this.parent.dLog && console.warn('Gain values cannot be greater than 1');
			val = 1;
		} else if (val < 0) {
			this.parent.dLog && console.warn('Gain values cannot be lower than 0');
			val = 0;
		}
		if (!fadeTime) {
			this.parent.dLog && console.warn(`Missing argument fadeTime. Set to 1 second`);
			fadeTime = 1;
		}
		this.dryGain.gain.cancelScheduledValues(this.context.currentTime);
		this.dryGain.gain.setValueAtTime(this.dryGain.gain.value, this.context.currentTime);
		this.dryGain.gain.linearRampToValueAtTime(val, this.context.currentTime + fadeTime);
	} else {
		this.parent.dLog && console.warn(`Add filter before trying to adjust dry gain [ID: ${this.parent.index}]`);
	}
};

/**
 * @function initConnections Create connections
 * @param {Object} node WebAudio node to init connections with (LP or HP)
 **/
Filter.prototype.initConnections = function (node) {
	this.dryGain.gain.setTargetAtTime(1, this.context.currentTime, 0.5);
	var d = this.parent.distortion.distorted;
	var b = this.parent.binauralizator.binauralized;
	// Do right connections
	if (d) {
		// Insert between master gain and distortion
		this.parent.masterGain.disconnect(this.parent.distortion.distNode);
		this.dryGain.connect(this.parent.distortion.distNode);
	} else if (b) {
		// Insert between master gain and binaural
		this.parent.masterGain.disconnect(this.parent.binauralizator.binauralFIRNode.input);
		this.dryGain.connect(this.parent.binauralizator.binauralFIRNode.input);
	} else {
		// Insert between master and destination
		this.parent.masterGain.disconnect(this.context.destination);
		this.dryGain.connect(this.context.destination);
	}
	node.connect(this.dryGain);
	this.parent.masterGain.connect(node);
	this.filtered = true;
};

/**
 * @function manageDisconnections Destroy connections
 * @param {Object} node WebAudio node to disconnect (LP or HP)
 **/
Filter.prototype.manageDisconnections = function (node) {
	this.parent.masterGain.disconnect(node);
	this.dryGain.disconnect();
	var d = this.parent.distortion.distorted;
	var b = this.parent.binauralizator.binauralized;
	// Redo connections
	if (d) {
		this.parent.masterGain.connect(this.parent.distortion.distNode);
	} else if (b) {
		this.parent.masterGain.connect(this.parent.binauralizator.binauralFIRNode.input);
	} else {
		this.parent.masterGain.connect(this.context.destination);
	}
	this.filtered = false;
	this.parent.dLog && console.log(`Last filter removed [ID: ${this.parent.index}]`);
};

/**
 * @function lowPassFilter
 * @param {Number} freq Cut frequency in Hz
 * @returns {Boolean} True if filter added / Flase if already present
 **/
Filter.prototype.lowPassFilter = function (freq) {
		if (freq < 40) {
			freq = 40;
			this.parent.dLog && console.warn('Cannot set filter freq under 40 Hz');
		} else if (freq > this.context.sampleRate/2) {
			freq = this.context.sampleRate/2;
			this.parent.dLog && console.warn('Cannot set filter freq over Nyquist limit');
		}
		if (!this.LPfiltered) {
			// Set frequency
			this.LPfilter.frequency.setTargetAtTime(freq, this.context.currentTime, 0.5);
			// Connect filter
			if (!this.HPfiltered) { // If not HP filtered init connections
				this.initConnections(this.LPfilter);
			} else { // If HP filtered already done
				this.parent.masterGain.disconnect(this.HPfilter);
				this.parent.masterGain.connect(this.LPfilter);
				this.LPfilter.connect(this.HPfilter);
			}
			this.LPfiltered = true;
			return true;
		} else {
			this.parent.dLog && console.warn(`Cannot add LPF. Sound already filterd [ID: ${this.parent.index}]`);
			return false;
		}
};

/**
 * @function setLPFrequency
 * @param {Number} freq Cut frequency in Hz
 * @param {Booleand} force If true, force set even if already fading
 **/
Filter.prototype.setLPFrequency = function (freq, force) {
	if (freq < 40) {
		freq = 40;
		this.parent.dLog && console.warn('Cannot set filter freq under 40 Hz');
	} else if (freq > this.context.sampleRate/2) {
		freq = this.context.sampleRate/2;
		this.parent.dLog && console.warn('Cannot set filter freq over Nyquist limit');
	}
	if (this.LPfiltered) {
		if (!this.LPfading) {
			this.LPfilter.frequency.setTargetAtTime(freq, this.context.currentTime, 0.5);
		} else if (force) {
			this.fadeToLPFrequency(freq, 1);
		} else {
			this.parent.dLog && console.warn(`Cannot set LPF freq. Filter freq is fading [ID: ${this.parent.index}]`);
		}
	} else {
		this.parent.dLog && console.warn(`Cannot set LPF freq. Sound not filtered [ID: ${this.parent.index}]`);
	}
};

/**
 * @function fadeToLPFrequency
 * @param {Number} freq Cut frequency in Hz
 * @param {Number} fadeTime fade time in seconds
 **/
Filter.prototype.fadeToLPFrequency = function (freq, fadeTime) {
	const self = this;
	if (freq < 40) {
		freq = 40;
		self.parent.dLog && console.warn(`Cannot fade filter freq under 40 Hz [ID: ${self.parent.index}]`);
	} else if (freq > self.context.sampleRate/2) {
		freq = self.context.sampleRate/2;
		self.parent.dLog && console.warn(`Cannot fade filter freq over Nyquist limit [ID: ${self.parent.index}]`);
	}
	if (!fadeTime) {
		self.parent.dLog && console.warn(`Missing argument fadeTime. Set to 1 second [ID: ${self.parent.index}]`);
		fadeTime = 1;
	}
	if (self.LPfiltered) {
		if (self.LPfading) {
			// If already fading reset fade
			self.LPfilter.frequency.cancelScheduledValues(self.context.currentTime);
			clearTimeout(self.LPflagReset);
		}
		self.LPfilter.frequency.setValueAtTime(self.LPfilter.frequency.value, self.context.currentTime);
		self.LPfilter.frequency.exponentialRampToValueAtTime(freq, self.context.currentTime + fadeTime);
		self.LPfading = true;
		self.LPflagReset = setTimeout(function () {
			self.LPfading = false;
			self.parent.dLog && console.log(`Freq fade finished`);
		}, fadeTime * 1000);
	} else {
		self.parent.dLog && console.warn(`Cannot fade LPF freq. Sound not filtered [ID: ${self.parent.index}]`);
	}
};

/**
 * @function getLPFrequency
 * @returns {Number} Cut frequenct in Hz
 **/
Filter.prototype.getLPFrequency = function () {
		if (this.LPfiltered) {
			return this.LPfilter.frequency.value;
		} else {
			this.parent.dLog && console.warn(`Cannot get LPF freq. Sound not filtered. [ID: ${this.parent.index}]`);
			return 0;
		}
};

/**
 * @function removeLowPassFilter
 * @returns {Boolean} True if filter removed / False if not
 **/
Filter.prototype.removeLowPassFilter = function () {
	const self = this;
	if (self.LPfiltered) {
		// fade freq for smooth effect
		self.fadeToLPFrequency(20000, 2);
		setTimeout(function () {
			// Disconnect filter
			self.LPfilter.disconnect();
			if (self.HPfiltered) {
				self.parent.masterGain.disconnect(self.LPfilter);
				self.parent.masterGain.connect(self.HPfilter);
			} else {
				self.manageDisconnections(self.LPfilter);
			}
			self.LPfiltered = false;
		}, 2000);
		return true;
	} else {
		self.parent.dLog && console.warn(`Cannot remove LPF. Sound not filtered [ID: ${this.parent.index}]`);
		return false;
	}
};

/**
 * @function highPassFilter
 * @param {Number} freq Cut frequency in Hz
 * @returns {Boolean} True if filter added / Flase if already present
 **/
Filter.prototype.highPassFilter = function (freq) {
		if (freq < 40) {
			freq = 40;
			this.parent.dLog && console.warn('Cannot set filter freq under 40 Hz');
		} else if (freq > this.context.sampleRate/2) {
			freq = this.context.sampleRate/2;
			this.parent.dLog && console.warn('Cannot set filter freq over Nyquist limit');
		}
		if (!this.HPfiltered) {
			// Set frequency
			this.HPfilter.frequency.setTargetAtTime(freq, this.context.currentTime, 0.5);
			// Connect filter
			if (!this.LPfiltered) { // If not LP filtered init connections
				this.initConnections(this.HPfilter);
			} else { // If LP filtered already done
				this.LPfilter.disconnect(this.dryGain);
				this.LPfilter.connect(this.HPfilter);
				this.HPfilter.connect(this.dryGain);
			}
			this.HPfiltered = true;
			return true;
		} else {
			this.parent.dLog && console.warn(`Cannot add HPF. Sound already filterd [ID: ${this.parent.index}]`);
			return false;
		}
};

/**
 * @function setHPFrequency
 * @param {Number} freq Cut frequency in Hz
 * @param {Booleand} force If true, force set even if already fading
 **/
Filter.prototype.setHPFrequency = function (freq, force) {
	if (freq < 40) {
		freq = 40;
		this.parent.dLog && console.warn('Cannot set filter freq under 40 Hz');
	} else if (freq > this.context.sampleRate/2) {
		freq = this.context.sampleRate/2;
		this.parent.dLog && console.warn('Cannot set filter freq over Nyquist limit');
	}
	if (this.HPfiltered) {
		if (!this.HPfading) {
			this.HPfilter.frequency.setTargetAtTime(freq, this.context.currentTime, 0.5);
		} else if (force) {
			this.fadeToHPFrequency(freq, 1);
		} else {
			this.parent.dLog && console.warn(`Cannot set HPF freq. Filter freq is fading [ID: ${this.parent.index}]`);
		}
	} else {
		this.parent.dLog && console.warn(`Cannot set HPF freq. Sound not filtered [ID: ${this.parent.index}]`);
	}
};

/**
 * @function fadeToHPFrequency
 * @param {Number} freq Cut frequency in Hz
 * @param {Number} fadeTime fade time in seconds
 **/
Filter.prototype.fadeToHPFrequency = function (freq, fadeTime) {
	const self = this;
	if (freq < 40) {
		freq = 40;
		self.parent.dLog && console.warn(`Cannot fade filter freq under 40 Hz [ID: ${self.parent.index}]`);
	} else if (freq > self.context.sampleRate/2) {
		freq = self.context.sampleRate/2;
		self.parent.dLog && console.warn(`Cannot fade filter freq over Nyquist limit [ID: ${self.parent.index}]`);
	}
	if (!fadeTime) {
		self.parent.dLog && console.warn(`Missing argument fadeTime. Set to 1 second [ID: ${self.parent.index}]`);
		fadeTime = 1;
	}
	if (self.HPfiltered) {
		if (self.HPfading) {
			// If already fading reset fade
			self.HPfilter.frequency.cancelScheduledValues(self.context.currentTime);
			clearTimeout(self.HPflagReset);
		}
		self.HPfilter.frequency.setValueAtTime(self.HPfilter.frequency.value, self.context.currentTime);
		self.HPfilter.frequency.exponentialRampToValueAtTime(freq, self.context.currentTime + fadeTime);
		self.HPfading = true;
		self.HPflagReset = setTimeout(function () {
			self.HPfading = false;
			self.parent.dLog && console.log(`Freq fade finished`);
		}, fadeTime * 1000);
	} else {
		self.parent.dLog && console.warn(`Cannot fade HPF freq. Sound not filtered [ID: ${self.parent.index}]`);
	}
};

/**
 * @function getHPFrequency
 * @returns {Number} Cut frequenct in Hz
 **/
Filter.prototype.getHPFrequency = function () {
		if (this.HPfiltered) {
			return this.HPfilter.frequency.value;
		} else {
			this.parent.dLog && console.warn(`Cannot get HPF freq. Sound not filtered [ID: ${this.parent.index}]`);
			return 0;
		}
};

/**
 * @function removeHighPassFilter
 * @returns {Boolean} True if filter removed / False if not
 **/
Filter.prototype.removeHighPassFilter = function () {
	const self = this;
	if (self.HPfiltered) {
		// Fade freq for smooth transition
		self.fadeToHPFrequency(40, 2);
		setTimeout(function () {
			// Disconnect filter
			self.HPfilter.disconnect();
			if (self.LPfiltered) {
				self.LPfilter.disconnect(self.HPfilter);
				self.LPfilter.connect(self.dryGain);
			} else {
				self.manageDisconnections(self.HPfilter);
			}
			self.HPfiltered = false;
		}, 2000);
		return true;
	} else {
		self.parent.dLog && console.warn(`Cannot remove HPF. Sound not filtered [ID: ${this.parent.index}]`);
		return false;
	}
};

// *********************************************************
// *********************************************************
/**
 * BINAURALIZATOR MODULE
 * @param {Object} AudioContext WebAudio Context of current application
 * @param {Object} parentNode parent audio AudioNode
 **/
function Binauralizator(audioContext, parentNode) {
	this.context = audioContext;
	this.parent = parentNode;
	// create convolver node
	this.binauralFIRNode = new BinauralFIR({
		audioContext: this.context
	});
	// set hrtf
	this.setHrtf();
	this.binauralized = false;
}

/**
 * Sets HRTF dataset in binaural node
**/
Binauralizator.prototype.setHrtf = function () {
	for (var i = 0; i < hrtfs.length; i++) {
		var buffer = this.context.createBuffer(2, 512, this.context.sampleRate);
		var bufferChannelLeft = buffer.getChannelData(0);
		var bufferChannelRight = buffer.getChannelData(1);
		for (var e = 0; e < hrtfs[i].fir_coeffs_left.length; e++) {
			bufferChannelLeft[e] = hrtfs[i].fir_coeffs_left[e];
			bufferChannelRight[e] = hrtfs[i].fir_coeffs_right[e];
		}
		hrtfs[i].buffer = buffer;
	}
	this.binauralFIRNode.HRTFDataset = hrtfs;
 };

 /**
  * @returns {Boolean} True if binaural added / False if already present
  **/
Binauralizator.prototype.makeBinaural = function () {
	if (this.parent instanceof StreamSound || this.parent instanceof BufferedSound) {
		if (!this.binauralized) {
			var f = this.parent.filter.filtered;
			var d = this.parent.distortion.distorted;
			// Do connections
			if (d) {
				this.parent.distortion.distNode.disconnect(this.context.destination);
				this.parent.distortion.distNode.connect(this.binauralFIRNode.input);
			} else if (f) {
				this.parent.filter.dryGain.disconnect(this.context.destination);
				this.parent.filter.dryGain.connect(this.binauralFIRNode.input);
			} else {
				this.parent.masterGain.disconnect(this.context.destination);
				this.parent.masterGain.connect(this.binauralFIRNode.input);
			}
			this.binauralFIRNode.connect(this.context.destination);
			this.binauralFIRNode.setPosition(0, 0, 1);
			this.binauralized = true;
			return true;
		} else {
			this.parent.dLog && console.warn(`Cannot make binaural. Sound already binauralized [ID: ${this.parent.index}]`);
			return false;
		}
	} else {
		this.parent.dLog && console.error('ERROR: wrong sound type in Binauralizator (only StreamSound accepted)');
	}
};

/**
 * @returns {Boolean} True if binaural removed / False if not present
 **/
Binauralizator.prototype.removeBinaural = function () {
	if (this.binauralized) {
		this.binauralFIRNode.disconnect(this.context.destination);
		var f = this.parent.filter.filtered;
		var d = this.parent.distortion.distorted;
		if (d) {
			this.parent.distortion.distNode.disconnect(this.binauralFIRNode.input);
			this.parent.distortion.distNode.connect(this.context.destination);
		} else if (f) {
			this.parent.filter.dryGain.disconnect(this.binauralFIRNode.input);
			this.parent.filter.dryGain.connect(this.context.destination);
		} else {
			this.parent.masterGain.disconnect(this.binauralFIRNode.input);
			this.parent.masterGain.connect(this.context.destination);
		}
		this.binauralized = false;
		return true;
	} else {
		this.parent.dLog && console.warn(`Cannot remove binaural. Sound not binauralized [ID: ${this.parent.index}]`);
		return false;
	}
};

/**
 * @param {Number} azimuth Binaural azimuth position in degrees
 **/
Binauralizator.prototype.setPosition = function (azimuth) {
	if (this.binauralized) {
		this.binauralFIRNode.setPosition(azimuth, 0, 1);
	} else {
		this.parent.dLog && console.warn(`Cannot set binaural position. Sound not binauralized [ID: ${this.parent.index}]`);
	}
};

/**
 * @param {Number} startPos Binaural azimuth position in degrees
 * @param {Number} endPos Binaural azimuth position in degrees
 **/
Binauralizator.prototype.interpolatePosition = function (startPos, endPos) {
	const self = this;
	if (self.binauralized) {
		var N = 0;
		(function smooth() {
			var tiny = 1 - (1/Math.pow(2,N));
			var pos = tiny * endPos + (1 - tiny) * startPos;
			self.setPosition(pos);
			N += 0.3;
			if (tiny < 0.9) {
				setTimeout(smooth, 50);
			}  else {
				self.setPosition(endPos);
			}
		}());
	} else {
		this.parent.dLog && console.warn(`Cannot set binaural position. Sound not binauralized [ID: ${this.parent.index}]`);
	}
};


/**
 * @returns {Number} Binaural azimuth position in degrees
 **/
Binauralizator.prototype.getPosition  = function () {
	if (this.binauralized) {
		return this.binauralFIRNode.getPosition().azimuth;
	} else {
		this.parent.dLog && console.warn(`Cannot get binaural position. Sound not binauralized [ID: ${this.parent.index}]`);
		return 0;
	}
};

// *********************************************************
// *********************************************************
/**
 * @function StereoPanner module -> to be used instead of BINAURAL on mobile
 * @param {Object} AudioContext WebAudio Context of current application
 * @param {Object} parentNode parent audio AudioNode
 **/
 function StereoPanner(audioContext, parentNode) {
	 this.context = audioContext;
	 this.parent = parentNode;
	 this.panNode = this.context.createStereoPanner();
	 this.stereoPanned = false;
 }

 /**
	* @returns {Boolean} True if binaural added / False if already present
	**/
StereoPanner.prototype.addSteroPanner = function () {
 if (this.parent instanceof StreamSound || this.parent instanceof BufferedSound) {
	 if (!this.stereoPanned) {
		 // var f = this.parent.filter.filtered;
		 // var d = this.parent.distortion.distorted;
		 // // Do connections
		 // if (d) {
			//  this.parent.distortion.distNode.disconnect(this.context.destination);
			//  this.parent.distortion.distNode.connect(this.panNode);
		 // } else if (f) {
			//  this.parent.filter.dryGain.disconnect(this.context.destination);
			//  this.parent.filter.dryGain.connect(this.panNode);
		 // } else {
			 this.parent.masterGain.disconnect(this.context.destination);
			 this.parent.masterGain.connect(this.panNode);
		 // }
		 this.panNode.connect(this.context.destination);
		 this.panNode.pan.setValueAtTime(0, this.context.currentTime);
		 this.stereoPanned = true;
		 return true;
	 } else {
		 this.parent.dLog && console.warn(`Cannot connect stereo panner. Sound already stereoPanned or panned [ID: ${this.parent.index}]`);
		 return false;
	 }
 } else {
	 this.parent.dLog && console.error('ERROR: wrong sound type in StereoPanner');
 }
};

/**
 * @returns {Boolean} True if stereo panner removed / False if not present
 **/
StereoPanner.prototype.removeStereoPanner = function () {
	if (this.stereoPanned) {
		this.panNode.disconnect(this.context.destination);
		// var f = this.parent.filter.filtered;
		// var d = this.parent.distortion.distorted;
		// if (d) {
		// 	this.parent.distortion.distNode.disconnect(this.panNode);
		// 	this.parent.distortion.distNode.connect(this.context.destination);
		// } else if (f) {
		// 	this.parent.filter.dryGain.disconnect(this.panNode);
		// 	this.parent.filter.dryGain.connect(this.context.destination);
		// } else {
			this.parent.masterGain.disconnect(this.panNode);
			this.parent.masterGain.connect(this.context.destination);
		// }
		this.stereoPanned = false;
		return true;
	} else {
		this.parent.dLog && console.warn(`Cannot remove stereo panner. Sound not panned [ID: ${this.parent.index}]`);
		return false;
	}
};

/**
 * @param {Number} value unitless value between -1 (full left) and 1 (full right)
 **/
StereoPanner.prototype.setPosition = function (value) {
	if (this.stereoPanned) {
		if (value < -1) {
			value = -1;
			self.parent.dLog && console.warn(`Stereo panner position cannot be < -1 [ID: ${self.parent.index}]`);
		} else if (value > 1) {
			value = 1;
			self.parent.dLog && console.warn(`Stereo panner position cannot be > 1 [ID: ${self.parent.index}]`);
		}
		// console.log(`setting position ${value}`);
		this.panNode.pan.setValueAtTime(value, this.context.currentTime);
	} else {
		this.parent.dLog && console.warn(`Cannot set stereo position. Sound not panned [ID: ${this.parent.index}]`);
	}
};

/**
 * @param {Number} startPos unitless value between -1 (full left) and 1 (full right)
 * @param {Number} endPos unitless value between -1 (full left) and 1 (full right)
 **/
StereoPanner.prototype.interpolatePosition = function (startPos, endPos) {
	const self = this;
	if (self.stereoPanned) {
		var N = 0;
		(function smooth() {
			var tiny = 1 - (1/Math.pow(2,N));
			var pos = tiny * endPos + (1 - tiny) * startPos;
			self.setPosition(pos);
			N += 0.13;
			if (tiny < 0.9) {
			// if (pos < endPos) {
				setTimeout(smooth, 25);
			}  else {
				self.setPosition(endPos);
			}
		}());
	} else {
		this.parent.dLog && console.warn(`Cannot set stereo position. Sound not panned [ID: ${this.parent.index}]`);
	}
};

/**
 * @returns {Number} Binaural azimuth position in degrees
 **/
StereoPanner.prototype.getPosition  = function () {
	if (this.stereoPanned) {
		return this.panNode.pan.value;
	} else {
		this.parent.dLog && console.warn(`Cannot get stereo position. Sound not panned [ID: ${this.parent.index}]`);
		return 0;
	}
};

// *********************************************************
// *********************************************************
/**
 * DISTORTION MODULE
 * @param {Object} AudioContext WebAudio Context of current application
 * @param {Object} parentNode parent audio AudioNode
 **/
 function Distortion(audioContext, parentNode) {
	 this.context = audioContext;
	 this.parent = parentNode;
	 this.distNode = this.context.createWaveShaper();
	 this.distorted = false;
 }

/**
 * @param {Number} amount Amount of distortion. Can basically be any positive number
 * @see {@link http://stackoverflow.com/a/22313408/1090298}
 **/
 Distortion.prototype.makeDistortionCurve = function (amount) {
	 var k = typeof amount === 'number' ? amount : 50,
	 	n_samples = 44100,
		curve = new Float32Array(n_samples),
    deg = Math.PI / 180,
    i = 0,
    x;
	for ( ; i < n_samples; ++i ) {
		x = i * 2 / n_samples - 1;
		curve[i] = ( 3 + k ) * x * 20 * deg / (Math.PI + k * Math.abs(x));
	}
  return curve;
}

/**
 * @param {Number} amount Amount of distortion
 * @returns {Boolean} True if dist added / False if already present
 **/
Distortion.prototype.addDistortion = function (amount) {
	if (amount > 500) {
		this.dLog && console.warn('Distortion amount cannot be more than 500');
		amount = 500;
	} else if (amount < 0) {
		this.dLog && console.warn('Distortion amount cannot be lower than 0');
		amount = 0;
	}
	if (!this.distorted) {
		this.distNode.curve = this.makeDistortionCurve(amount);
		// Do connections
		var f = this.parent.filter.filtered;
		var b = this.parent.binauralizator.binauralized;
		if (f && b) {
			// Filtered and binauralized
			this.parent.filter.dryGain.disconnect(this.parent.binauralizator.binauralFIRNode.input);
			this.parent.filter.dryGain.connect(this.distNode);
			this.distNode.connect(this.parent.binauralizator.binauralFIRNode.input);
		} else if (!f && !b) {
			// Not filtered and not binauralized
			this.parent.masterGain.disconnect(this.context.destination);
			this.parent.masterGain.connect(this.distNode);
			this.distNode.connect(this.context.destination);
		} else if (f) {
			// Filtered and not binauralized
			this.parent.filter.dryGain.disconnect(this.context.destination);
			this.parent.filter.dryGain.connect(this.distNode);
			this.distNode.connect(this.context.destination);
		} else {
			// Not filtered and binauralized
			this.parent.masterGain.disconnect(this.parent.binauralizator.binauralFIRNode.input);
			this.parent.masterGain.connect(this.distNode);
			this.distNode.connect(this.parent.binauralizator.binauralFIRNode.input);
		}
		this.distorted = true;
		return true;
	} else {
		this.parent.dLog && console.warn(`Cannot add distorion. Sound already distorted [ID: ${this.parent.index}]`);
		return false;
	}
};

/**
 * @param {Number} amount Amount of distortion
 **/
Distortion.prototype.setDistAmount = function (amount) {
	if (amount > 500) {
		this.dLog && console.warn('Distortion amount cannot be more than 500');
		amount = 500;
	} else if (amount < 0) {
		this.dLog && console.warn('Distortion amount cannot be lower than 0');
		amount = 0;
	}
	if (this.distorted) {
		this.distNode.curve = this.makeDistortionCurve(amount);
	} else {
		this.parent.dLog && console.warn(`Cannot set dist amount. Sound not distorted [ID: ${this.parent.index}]`);
	}
};

/**
 * @returns {Boolean} True if dist removed / False if not
 **/
Distortion.prototype.removeDistortion = function () {
	if (this.distorted) {
		var f = this.parent.filter.filtered;
		var b = this.parent.binauralizator.binauralized;
		this.distNode.disconnect();
		if (f && b) {
			// Filtered and binauralized
			this.parent.filter.dryGain.disconnect(this.distNode);
			this.parent.filter.dryGain.connect(this.parent.binauralizator.binauralFIRNode.input);
		} else if (!f && !b) {
			// Not filtered and not binauralized
			this.parent.masterGain.disconnect(this.distNode);
			this.parent.masterGain.connect(this.context.destination);
		} else if (f) {
			// Filtered and not binauralized
			this.parent.filter.dryGain.disconnect(this.distNode);
			this.parent.filter.dryGain.connect(this.context.destination);
		} else {
			// Not filtered and binauralized
			this.parent.masterGain.disconnect(this.distNode);
			this.parent.masterGain.connect(this.parent.binauralizator.binauralFIRNode.input);
		}
		this.distorted = false;
		return true;
	} else {
		this.parent.dLog && console.warn(`Cannot remove distortion. Sound not distorted [ID: ${this.parent.index}]`);
		return false;
	}
};

// *********************************************************
// *********************************************************
/** REVERBERATOR
 * @param {Object} AudioContext WebAudio Context of current application
 **/
function Reverberator(audioContext) {
	'use strict';
	this.context = audioContext;
	this.seconds = 1; // TEMP hard coded
	this.decay = 2; // TEMP hard coded
	this.convolver = '';
	// this.reverbImpulseURL = '../shared_media/audio/reverb-impulse/AbernyteGrainSilo.m4a';
	this.init();
}

Reverberator.prototype.init = function () {
	const self = this;
	// Reverb method 1
	self.convolver = self.context.createConvolver();
	self.convolver.buffer = self.buildImpulse();
	self.convolver.connect(self.context.destination);
	// Reverb method 2
	// self.convolver = self.context.createReverbFromUrl(self.reverbImpulseURL, function () {
	// 	self.convolver.connect(self.context.destination);
	// });
};

Reverberator.prototype.disconnectReverb = function () {
	this.convolver.disconnect();
};

/**
 * @returns {Object} WebAudioAPI buffer - A buffer with impulse for convolution reverb
 **/
Reverberator.prototype.buildImpulse = function () {
  var rate = this.context.sampleRate;
  var length = rate * this.seconds;
  var impulse = this.context.createBuffer(2, length, rate);
  var impulseL = impulse.getChannelData(0);
  var impulseR = impulse.getChannelData(1);
  for (var i = 0; i < length; i++) {
    impulseL[i] = (Math.random() * 2 - 1) * Math.pow(1 - i / length, this.decay);
    impulseR[i] = (Math.random() * 2 - 1) * Math.pow(1 - i / length, this.decay);
  }
  return impulse;
};
